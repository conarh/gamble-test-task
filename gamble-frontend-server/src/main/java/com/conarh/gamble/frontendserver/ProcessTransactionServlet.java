package com.conarh.gamble.frontendserver;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.conarh.gamble.common.vo.TransactionRequest;
import com.conarh.gamble.common.vo.TransactionResponse;
import com.conarh.gamble.frontendserver.vo.FrontendTransactionRequest;
import com.conarh.gamble.frontendserver.vo.FrontendTransactionResponse;

/**
 * Simple servlet to process rest calls from UI.
 * It makes randomized request to wallet server, logs results
 *
 * There is no need to use Spring here, as process logic is very simple
 *
 * @author Vladimier Khrystianovskyy
 */
@WebServlet(urlPatterns = "/rest/transaction",
	initParams = {@WebInitParam(name="wallet.server.url", value="http://localhost:8080/gamble-wallet")}
)
public class ProcessTransactionServlet extends HttpServlet{
    private final ObjectMapper mapper = new ObjectMapper();
    private final Random rnd = new Random();
    private static final String WALLET_UPDATE_PATH = "/rest/wallet/update";
    private final Logger log = LoggerFactory.getLogger(ProcessTransactionServlet.class);
    private final Map<String, Long> lastTransactionId = new ConcurrentHashMap<>();
    private final NumberFormat format = new DecimalFormat("0.00");
     
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final FrontendTransactionRequest request = mapper.readValue(req.getInputStream(), FrontendTransactionRequest.class);
        final TransactionRequest transactionRequest = generateRandomTransactionRequest(request);
        final TransactionResponse transactionResponse = callWalletServer(transactionRequest);
        final FrontendTransactionResponse response = new FrontendTransactionResponse();
        response.setUsername(transactionRequest.getUsername());
        if (transactionResponse.getError() == null){
        	lastTransactionId.put(transactionRequest.getUsername(), transactionResponse.getTransactionId()); 
        }
        try {
			BeanUtils.copyProperties(response, transactionResponse);
		} catch (IllegalAccessException | InvocationTargetException  e) {
			throw new ServletException("Error copying response properties", e);
		} 
        resp.setContentType(ContentType.APPLICATION_JSON.getMimeType());
        mapper.writeValue(resp.getOutputStream(), response);
        logBalanceChange(response);
    }
    
    private TransactionRequest generateRandomTransactionRequest(FrontendTransactionRequest request){
    	final String username = request.getUsernames()[rnd.nextInt(request.getUsernames().length)];
    	long transactionId = Math.abs(rnd.nextLong());
    	if (lastTransactionId.containsKey(username) && (rnd.nextInt(100) < 10)){
    		//approx 10% chance to get duplicate id
    		transactionId = lastTransactionId.get(username);
    	}
        final BigDecimal change = new BigDecimal(rnd.nextInt((int)(request.getChangeLimit()*100))*(rnd.nextBoolean() ? 1 : -1)/100.0);
        final TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setUsername(username);
        transactionRequest.setTransactionId(transactionId);
        transactionRequest.setChange(change);
        return transactionRequest;
    }
    
    private TransactionResponse callWalletServer(TransactionRequest request) throws IOException{
   		final String jsonString = mapper.writeValueAsString(request);
    	return Request.Post(String.format("%s%s", getInitParameter("wallet.server.url"), WALLET_UPDATE_PATH))
    		.bodyString(jsonString, ContentType.APPLICATION_JSON)
    		.execute().handleResponse(new ResponseHandler<TransactionResponse>(){

				@Override
				public TransactionResponse handleResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException {
					return mapper.readValue(httpResponse.getEntity().getContent(), TransactionResponse.class);
				}
			});
    }
    
    private void logBalanceChange(FrontendTransactionResponse response){
    	if (response.getError() != null){
    		log.info("Player {} transactionID={} error={}", response.getUsername(), response.getTransactionId(), response.getError());
    	} else {
    		final String signum = response.getChange().signum() > 0 ? "IN" : "OUT";
    		log.info("Player {} transactionID={} change={} {} balance={} version={}", response.getUsername(), response.getTransactionId(), signum, format.format(response.getChange().abs()), format.format(response.getBalance()), response.getVersion());
    	}
    }
}
