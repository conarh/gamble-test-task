package com.conarh.gamble.frontendserver.vo;

/**
 * Transaction request from frontend containing username
 *
 * @author Vladimier Khrystianovskyy
 */
public class FrontendTransactionRequest {
    private String[] usernames;
    private double changeLimit;

    public double getChangeLimit() {
        return changeLimit;
    }

    public void setChangeLimit(double changeLimit) {
        this.changeLimit = changeLimit;
    }

    public String[] getUsernames() {
        return usernames;
    }

    public void setUsernames(String[] usernames) {
        this.usernames = usernames;
    }
}
