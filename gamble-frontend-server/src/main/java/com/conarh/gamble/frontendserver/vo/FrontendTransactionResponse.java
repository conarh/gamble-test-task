package com.conarh.gamble.frontendserver.vo;

import com.conarh.gamble.common.vo.TransactionResponse;

/**
 * @author Vladimier Khrystianovskyy
 */
public class FrontendTransactionResponse extends TransactionResponse {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
