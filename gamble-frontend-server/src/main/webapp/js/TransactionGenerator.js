/**
 * Simple JQuery-based transaction updates generator
 */
var transactionGenerator = new TransactionGenerator();

function TransactionGenerator() {}

TransactionGenerator.prototype.init = function() {
    $("body").on("click", "#updateWallet", this.startGeneration.bind(this));
    this.tableBody = $("#walletLog tbody");
    console.log("TransactionGenerator initialized");
};

TransactionGenerator.prototype.startGeneration = function() {
    var
        i,
        request
    ;
    this.reset();
    request = {
        usernames : this.usernames.split(','),
        changeLimit : this.maxChange
    };
    for (i = 0; i < this.count; i++) {
        $.ajax({
            method : "POST",
            url : "rest/transaction",
            dataType : "json",
            data : JSON.stringify(request),
            success : this.logTransaction.bind(this),
            error : function(ajax, status, error){
                console.warn("Transaction request error: "+error);
            }
        });
    }
};

TransactionGenerator.prototype.logTransaction = function(response){
    var
    	row = {
	        username : response.username,
	        id : response.transactionId,
	        error : response.error,
	        version : response.version,		
	        change : response.change,
	        balance : response.balance
    	},
    	field
    ;
    for (field in row){
    	if (row[field] == null){
    		row[field] = "";
    	}
    }
    this.tableBody.prepend("<tr><td>" + row.username + "</td><td>" + row.id + "</td><td>" + row.error + "</td><td>" 
    		+ row.version +"</td><td>" + row.change + "</td><td>" + row.balance + "</td></tr>");
};

TransactionGenerator.prototype.reset = function() {
    this.tableBody.empty();
    this.usernames = $("#username").val();
    this.maxChange = parseFloat($("#maxChange").val());
    this.count = parseInt($("#count").val())
};

$(document).ready(function(){transactionGenerator.init();});
