package com.conarh.gamble.common.vo;

import java.math.BigDecimal;

import com.conarh.gamble.common.TransactionError;

/**
 * Response from wallet server back to front-end server
 * 
 * @author Conarh
 *
 */
public class TransactionResponse {
	private long transactionId;
	private TransactionError error;
	private Long version;
    private BigDecimal change;
    private BigDecimal balance;
    
	public long getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	
	public TransactionError getError() {
		return error;
	}
	
	public void setError(TransactionError error) {
		this.error = error;
	}
	
	public Long getVersion() {
		return version;
	}
	
	public void setVersion(Long version) {
		this.version = version;
	}
	
	public BigDecimal getChange() {
		return change;
	}
	
	public void setChange(BigDecimal change) {
		this.change = change;
	}
	
	public BigDecimal getBalance() {
		return balance;
	} 
	
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
    
}
