package com.conarh.gamble.common;

/**
 * Represents transaction error which is returned by server
 *
 * @author Vladimier Khrystianovskyy
 */
public enum TransactionError {
    BALANCE_NEGATIVE,
    TRANSACTION_DUPLICATE,
    BALANCE_LIMIT_BREACH,
    BLACKLISTED_USER;

}
