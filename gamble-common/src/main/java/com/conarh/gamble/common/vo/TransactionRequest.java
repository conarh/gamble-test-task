package com.conarh.gamble.common.vo;

import java.math.BigDecimal;

/**
 * Represents request for transaction from front-end server
 *
 * @author Vladimier Khrystianovskyy
 */
public class TransactionRequest {
    private String username;
    private long transactionId;
    private BigDecimal change;

    public BigDecimal getChange() {
        return change;
    }

    public void setChange(BigDecimal change) {
        this.change = change;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
