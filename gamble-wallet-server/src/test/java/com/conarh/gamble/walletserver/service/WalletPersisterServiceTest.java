package com.conarh.gamble.walletserver.service;

import static com.conarh.gamble.walletserver.dao.PlayerRepositoryTest.DOUBLE_PRESCISION;
import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.easymock.IAnswer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.conarh.gamble.walletserver.dao.PlayerRepository;
import com.conarh.gamble.walletserver.domain.Player;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:test-wallet-persister.xml")
public class WalletPersisterServiceTest {
	@Autowired
	private WalletManager manager;
	@Autowired
	private PlayerRepository repo;
	@Resource(name="persistInterval")
	private Long persistInterval; 
	private Capture<Iterable<Player>> playersUpdate = Capture.newInstance();
	private int findPlayerCallNumber = 0;
	
	@Test	
	public void testPeriodicPersistence() throws InterruptedException{
		final Map<String, Wallet> snapshot1 = initSnapshot(new WalletsMapBuilder().addWallet("Player1", 1000, 0));
		final Map<String, Wallet> snapshot2 = initSnapshot(new WalletsMapBuilder()
		   .addWallet("Player1", 2000, 1)
		   .addWallet("Player2", 1000, 0));
		expect(repo.findByUsername(anyString())).andAnswer(new IAnswer<Player>(){

			@Override
			public Player answer() throws Throwable {
				final String username = (String) EasyMock.getCurrentArguments()[0];
				final Player p = new Player();
				p.setUsername(username);
				return p;
			}}).times(findPlayerCallNumber);
		expect(manager.getWallets()).andReturn(Maps.<String, Wallet>newHashMap()).anyTimes();
		replay(manager, repo);
		assertPersistCall(snapshot1);
		assertPersistCall(snapshot2);
		verify(manager, repo);
	}
	
	private Map<String, Wallet> initSnapshot(WalletsMapBuilder builder){
		Map<String, Wallet> wallets = builder.build();
		expect(manager.getWallets()).andReturn(wallets);
		expect(repo.save(capture(playersUpdate))).andReturn(Sets.<Player>newHashSet());
		findPlayerCallNumber+=wallets.size();
		return wallets;
	}
		
	private void assertPersistCall(Map<String, Wallet> snapshot) throws InterruptedException{
		Thread.sleep((long)(persistInterval*1.5));
		final Set<Player> players = Sets.newHashSet(playersUpdate.getValue());
		assertEquals("Player amount does not match wallet amount", snapshot.size(), players.size());
		for (Player p : players){
			assertTrue(String.format("Player does not found: %s", p.getUsername()), snapshot.containsKey(p.getUsername()));
			final Wallet wallet = snapshot.get(p.getUsername());
			assertEquals(String.format("Invalid balance for: %s", p.getUsername()), p.getBalance().doubleValue(), wallet.getAmount().doubleValue(), DOUBLE_PRESCISION);
			assertEquals(String.format("Invalid version for: %s", p.getUsername()), p.getVersion(), wallet.getVersion());
		}
	}
	
	private static class WalletsMapBuilder{
		private Map<String, Wallet> wallets = Maps.newHashMap();
		
		public WalletsMapBuilder addWallet(String user, double balance, long version){
			final Wallet wallet = new Wallet();
			wallet.setAmount(new BigDecimal(balance));
			wallet.setVersion(version);
			wallets.put(user, wallet);
			return this;
		}
		
		public Map<String, Wallet> build(){
			return wallets;
		}
	} 
}
