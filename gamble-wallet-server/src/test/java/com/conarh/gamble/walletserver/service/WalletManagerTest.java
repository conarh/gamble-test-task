package com.conarh.gamble.walletserver.service;

import static com.conarh.gamble.common.TransactionError.BALANCE_LIMIT_BREACH;
import static com.conarh.gamble.common.TransactionError.BALANCE_NEGATIVE;
import static com.conarh.gamble.common.TransactionError.BLACKLISTED_USER;
import static com.conarh.gamble.common.TransactionError.TRANSACTION_DUPLICATE;
import static com.conarh.gamble.walletserver.dao.PlayerRepositoryTest.DOUBLE_PRESCISION;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.conarh.gamble.common.TransactionError;
import com.conarh.gamble.common.vo.TransactionRequest;
import com.conarh.gamble.walletserver.dao.PlayerRepository;
import com.conarh.gamble.walletserver.domain.Player;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:test-wallet.xml")
public class WalletManagerTest {
	private static final double BALANCE_LIMIT = 10000;
	private static final double BALANCE = 9000;
	private static final String PLAYER = "ExistingPlayer";
	private static final long VERSION = 2;
	private static final String NEW_PLAYER = "NewPlayer";
	private static final String BANNED_PLAYER = "Banned";
	private static final String BANNED_PLAYER2 = "Banned2";

	@Resource(name="considerableTransactionCount")
	private Integer considerableTransactionCount;
	
	@Autowired
	private PlayerRepository repo;
	
	@Autowired
	private WalletManager walletManager;
	
	@Before
	public void init(){
		final Player player = new Player();
		player.setUsername(PLAYER);
		player.setBalance(new BigDecimal(BALANCE));
		player.setVersion(VERSION);
		expect(repo.findByUsername(PLAYER)).andReturn(player).times(0, 1);
		expect(repo.findByUsername(NEW_PLAYER)).andReturn(null).times(0, 1);
		replay(repo);
		walletManager.setBalanceLimit(new BigDecimal(BALANCE_LIMIT));
		walletManager.setBlacklist(StringUtils.join(new String[]{BANNED_PLAYER, BANNED_PLAYER2}, ","));
	}
	
	@After
	public void reset(){
		verify(repo);
		EasyMock.reset(repo);
		((Map<?, ?>)ReflectionTestUtils.getField(walletManager, "wallets")).clear();
	}
	
	@Test
	public void testWalletBalanceExistingUser(){
		final double delta = BALANCE_LIMIT-1;
		assertAddTransaction(PLAYER, delta, 1, BALANCE+delta, VERSION+1, null);
	}
	
	@Test
	public void testWalletBalanceNewUser(){
		final double delta = BALANCE_LIMIT-1;
		assertAddTransaction(NEW_PLAYER, delta, 1, delta, 1L, null);
	}
	
	@Test
	public void testWalletBannedUser(){
		assertAddTransaction(BANNED_PLAYER, 0, 1, null, null, BLACKLISTED_USER);
		assertAddTransaction(BANNED_PLAYER2, 0, 1, null, null, BLACKLISTED_USER);
	}
	
	@Test
	public void testLimitBreach(){
		double delta = BALANCE_LIMIT+1;
		assertAddTransaction(PLAYER, delta, 1, null, null, BALANCE_LIMIT_BREACH);
		delta = -(BALANCE_LIMIT+1);
		assertAddTransaction(PLAYER, delta, 1, null, null, BALANCE_LIMIT_BREACH);
	}
	
	@Test
	public void testNegativeBalance(){
		double delta = -(BALANCE+1);
		assertAddTransaction(PLAYER, delta, 1, null, null, BALANCE_NEGATIVE);
	}
	
	@Test
	public void testDuplicateTransaction(){
		assertAddTransaction(PLAYER, 1, 1, BALANCE+1, VERSION+1, null);
		for (int i=1; i<considerableTransactionCount+1; i++){
			assertAddTransaction(PLAYER, 1, i+1, BALANCE+1+i, VERSION+1+i, null);
			if (i<considerableTransactionCount){
				assertAddTransaction(PLAYER, 1, 1, BALANCE+1+i, VERSION+1+i, TRANSACTION_DUPLICATE);
			} else {
				assertAddTransaction(PLAYER, 1, 1, BALANCE+1+i+1, VERSION+1+i+1, null);
			}
		}
	}
	
	private void assertAddTransaction(String player, double delta, long id, Double result, Long version, TransactionError error){
		final WalletManagerServiceResponse response = walletManager.addTransaction(getTransactionRequest(player, new BigDecimal(delta), id));
		if (error == null || error == TRANSACTION_DUPLICATE){
			if (error == TRANSACTION_DUPLICATE){
				assertEquals("Invalid error code", TRANSACTION_DUPLICATE, response.getError());
			} else {
				assertNull("Error present", response.getError());
			}
			assertEquals("Invalid final balance", new BigDecimal(result).doubleValue(), response.getBalance().doubleValue(), DOUBLE_PRESCISION);
			assertEquals("Invalid balance version", version, response.getVersion());
		} else {
			assertNull("Final balance present", response.getBalance());
			assertNull("Balance version present", response.getVersion());
			assertEquals("Invalid error", error, response.getError());
		}
	}
	
	private TransactionRequest getTransactionRequest(String username, BigDecimal delta, long id){
		final TransactionRequest request = new TransactionRequest();
		request.setUsername(username);
		request.setChange(delta);
		request.setTransactionId(id);
		return request;
	}
}
