package com.conarh.gamble.walletserver.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import com.conarh.gamble.walletserver.domain.Player;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:test-dao.xml")
@Transactional
public class PlayerRepositoryTest {
	private static final int PLAYERS_NUM = 10;
	public static final double DOUBLE_PRESCISION = 0.001;
	
	@Autowired
	private PlayerRepository repo;
	
	@Autowired
	private JdbcTemplate jdbcTemplate; 
	
	@Test
	public void testPlayerMapping() {
		assertEquals("Initial table not empty", 0, repo.count());
		for (int i = 1; i<=PLAYERS_NUM; i++){
			final Player p = new Player(); 
			p.setUsername("Player"+i);
			p.setBalance(new BigDecimal(i));
			repo.save(p);
		}
		assertEquals("Invalid table size", PLAYERS_NUM, repo.count());
		final List<Player> playersFromDb = jdbcTemplate.query("SELECT * FROM player", new RowMapper<Player>(){

			@Override
			public Player mapRow(ResultSet res, int rowNum) throws SQLException {
				final Player p = new Player();
				ReflectionTestUtils.setField(p, "id", res.getLong("player_id"));
				ReflectionTestUtils.setField(p, "version", res.getLong("balance_version"));
				p.setBalance(res.getBigDecimal("balance"));
				p.setUsername(res.getString("username"));
				return p;
			}
			
		});
		assertEquals("Invalid number of players retreived", PLAYERS_NUM, playersFromDb.size());
		for (Player p : playersFromDb){
			assertPlayerInDatabase(p);
		}
	}
	
	@Test
	public void testPlayerBatchUpdate(){
		final Player p1 = createPlayerWithJdbc("Player1", 1000);
		assertEquals("Invalid number of players", 1, repo.count());
		assertPlayerInDatabase(p1);
		Player p2 = new Player();
		p2.setUsername("Player2");
		p2.setBalance(new BigDecimal(3000));
		p1.setBalance(new BigDecimal(2000));
		p1.setVersion(2);
		repo.save(Arrays.asList(p1, p2));
		assertEquals("Invalid number of players", 2, repo.count());
		final Player actualp1 = repo.findOne(p1.getId());
		assertTrue("Player1 does not exist", (actualp1 != null) && (actualp1.getId()>0));
		assertPlayer(actualp1, p1.getUsername(), p1.getBalance(), 2);
		final Player actualp2 = repo.findByUsername("Player2");
		assertTrue("Player2 does not exist", (actualp2 != null) && (actualp2.getId()>0));   
		assertPlayer(actualp2, p2.getUsername(), p2.getBalance(), 0);
	}
	
	@Test
	public void testFindByUsername(){
		createPlayerWithJdbc("TestPlayer", 123);
		Player p = repo.findByUsername("TestPlayer");
		assertNotNull("Existing player not found", p);
		p = repo.findByUsername("InvalidUsername");
		assertNull("Not existing player found", p);
	}
	
	private Player createPlayerWithJdbc(String username, double balance){
		final SimpleJdbcInsert playerInsert = new SimpleJdbcInsert(jdbcTemplate).withTableName("player").usingGeneratedKeyColumns("id")
				.usingColumns("username", "balance", "balance_version");
		final long id = (Long) playerInsert.executeAndReturnKey(new MapSqlParameterSource().addValue("username", username).addValue("balance", new BigDecimal(balance)).addValue("balance_version", 1));
		return createPlayer(id, username, balance, 1);
	}
	
	private void assertPlayerInDatabase(Player p){
		final Player entity = repo.findOne(p.getId());
		assertNotNull(String.format("Player does not exist for id %d", p.getId()), entity);
		assertPlayer(p, entity.getUsername(), entity.getBalance(), entity.getVersion());
	}
	
	private void assertPlayer(Player p, String username, BigDecimal balance, long version){
		assertEquals("Invalid name", username, p.getUsername());
		assertEquals("Invalid balance_version", version, p.getVersion());
		assertEquals("Invalid balance", balance.doubleValue(), p.getBalance().doubleValue(), DOUBLE_PRESCISION);
	}
	
	private Player createPlayer(long id, String username, double balance, int version){
		final Player p = new Player();
		p.setUsername(username);
		p.setBalance(new BigDecimal(balance));
		ReflectionTestUtils.setField(p, "id", id);
		ReflectionTestUtils.setField(p, "version", version);
		return p;
	}
}