package com.conarh.gamble.walletserver.service;

import com.conarh.gamble.common.TransactionError;

import java.math.BigDecimal;

/**
 * Contains response of WalletManagerService
 *
 * @author Vladimier Khrystianovskyy
 */
public class WalletManagerServiceResponse {
    private BigDecimal balance;
    private Long version;
    private TransactionError error = null;

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public TransactionError getError() {
        return error;
    }

    public void setError(TransactionError error) {
        this.error = error;
    }

	public Long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}
    
}
