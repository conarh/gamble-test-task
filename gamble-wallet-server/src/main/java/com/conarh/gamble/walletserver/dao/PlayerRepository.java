package com.conarh.gamble.walletserver.dao;

import org.springframework.data.repository.CrudRepository;

import com.conarh.gamble.walletserver.domain.Player;

public interface PlayerRepository extends CrudRepository<Player, Long>{
	
	Player findByUsername(String username);
}
