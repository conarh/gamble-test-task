package com.conarh.gamble.walletserver.service;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
 
/**
 * Service which serve as a bridge between JAX-RS and Spring, by utilising Singleton creation 
 * and providing WalletManager to rest controllers  
 * 
 * @author Conarh
 *
 */
public class WalletManagerRestFacade{
	private static final Logger LOG = LoggerFactory.getLogger(WalletManagerRestFacade.class);
	private static WalletManagerRestFacade instance;
	@Autowired
	private WalletManager walletManager;
	
	//To prevent external instance creation
	private WalletManagerRestFacade(){}
	
	public synchronized static WalletManagerRestFacade instance(){
		if (instance == null){
			instance = new WalletManagerRestFacade();
			LOG.info("WalletManagerRestFacade created");
		}
		return instance;
	}
	
	/**
	 * Return live instance of WalletManager. Throws exception
	 * if WalletManager has not yet been created (e.g. Spring context has not been fully started)
	 * 
	 * @return
	 */
	public WalletManager getWalletManager(){
		if (walletManager == null){
			throw new IllegalStateException("WalletManagerRestFacade not initialized yet");
		} else {
			return walletManager;
		}
	}

	@PostConstruct
	public void init(){
		LOG.info("WalletManagerRestFacade initialized");
	}
}
