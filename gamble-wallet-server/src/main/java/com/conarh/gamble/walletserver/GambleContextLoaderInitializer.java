package com.conarh.gamble.walletserver;

import org.springframework.web.context.AbstractContextLoaderInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;

/**
 * Root context initializer for web container.
 * Utitlize web.xml-free way to load web context.
 *  
 * @author Conarh
 */
public class GambleContextLoaderInitializer extends AbstractContextLoaderInitializer {

	@Override
	protected WebApplicationContext createRootApplicationContext() {
		final XmlWebApplicationContext ctx = new XmlWebApplicationContext();
		ctx.setConfigLocation("classpath:application.xml");
		return ctx;
	}

}
