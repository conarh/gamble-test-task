package com.conarh.gamble.walletserver.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.http.MediaType;

import com.conarh.gamble.common.vo.TransactionRequest;
import com.conarh.gamble.common.vo.TransactionResponse;
import com.conarh.gamble.walletserver.service.WalletManagerRestFacade;
import com.conarh.gamble.walletserver.service.WalletManagerServiceResponse;

/**
 * Service to receive request for transaction from front-end server
 * 
 * @author Conarh
 *
 */
@Path("/wallet")
public class WalletTransactionRestService {
	private final WalletManagerRestFacade walletManagerFacade = WalletManagerRestFacade.instance();

	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	@Consumes(MediaType.APPLICATION_JSON_VALUE)
	public TransactionResponse addTransaction(TransactionRequest request){
		final TransactionResponse response = new TransactionResponse();
		response.setTransactionId(request.getTransactionId());
		final WalletManagerServiceResponse serviceResponse = walletManagerFacade.getWalletManager().addTransaction(request);
		response.setBalance(serviceResponse.getBalance());
		response.setChange(request.getChange());
		response.setVersion(serviceResponse.getVersion());
		response.setError(serviceResponse.getError());
		return response;
	}
}
