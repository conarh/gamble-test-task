package com.conarh.gamble.walletserver.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class JaxRsGambleApplication extends Application{

}
