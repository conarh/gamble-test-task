package com.conarh.gamble.walletserver.service;

import java.math.BigDecimal;
import java.util.List;

import com.conarh.gamble.common.TransactionError;
import com.google.common.collect.Lists;

/**
 * Manages user's wallet
 * 
 * @author Vladimier Khrystianovskyy
 */
public class Wallet {
	private int considerableTransactionCount;
    private List<Long> transactionIds = Lists.newArrayList();
    private BigDecimal amount = BigDecimal.ZERO;
    private long version = 0;
    
    public void setConsiderableTransactionCount(int considerableTransactionCount) {
		this.considerableTransactionCount = considerableTransactionCount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
     * Adds transaction if such does not exists.
     * @return null if transaction is added, error otherwise
     */
    public TransactionError addTransaction(long id, BigDecimal delta){
        TransactionError error = null;
        if (transactionIds.contains(id)){
            error = TransactionError.TRANSACTION_DUPLICATE;
        } else if (amount.add(delta).signum()<0){
            error = TransactionError.BALANCE_NEGATIVE;
        } else {
            amount = amount.add(delta);
            if (transactionIds.size() >= considerableTransactionCount){
                transactionIds.remove(0);
            }
            transactionIds.add(id);
            version++;
        }
        return error;
    }

    public BigDecimal getAmount() {
        return amount;
    }

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}
    
}
