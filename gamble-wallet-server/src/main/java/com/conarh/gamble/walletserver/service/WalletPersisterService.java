package com.conarh.gamble.walletserver.service;

import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.conarh.gamble.walletserver.dao.PlayerRepository;
import com.conarh.gamble.walletserver.domain.Player;
import com.google.common.collect.Sets;

/**
 * This service implements functionality of persisting players' wallets to database
 *   
 * @author Conarh
 */

public class WalletPersisterService {
	private final Logger log = LoggerFactory.getLogger(WalletPersisterService.class);
	private long persistInterval;
	@Autowired
	private PlayerRepository repo;
	@Autowired
	private WalletManager walletManager;
	//Daemon timer, no need to stop it
	private final Timer timer = new Timer(true);
	
	@PostConstruct
	public void init(){
		//TODO: Start persistence timer here
		timer.schedule(new TimerTask(){

			@Override
			public void run() {
				persistAllWallets();
			}}, persistInterval, persistInterval);
	}
	
	private void persistAllWallets(){
		final Map<String, Wallet> wallets = walletManager.getWallets();
		final Set<Player> playersToUpdate = Sets.newHashSet(); 
		for (String username : wallets.keySet()){
			//Prevents update of wallet during we persist it
			synchronized (username.intern()){
				final Wallet wallet = wallets.get(username);
				Player p = repo.findByUsername(username);
				if (p == null){
					p = new Player();
					p.setUsername(username);
				}
				p.setBalance(wallet.getAmount());
				p.setVersion(wallet.getVersion());
				playersToUpdate.add(p);
			}
		}
		if (!playersToUpdate.isEmpty()){
			repo.save(playersToUpdate);
			log.info("{} players persisted", playersToUpdate.size());
		}
	}

	public void setPersistInterval(long persistInterval) {
		this.persistInterval = persistInterval;
	}
	
}
