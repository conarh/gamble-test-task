package com.conarh.gamble.walletserver.service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;

import com.conarh.gamble.common.TransactionError;
import com.conarh.gamble.common.vo.TransactionRequest;
import com.conarh.gamble.walletserver.dao.PlayerRepository;
import com.conarh.gamble.walletserver.domain.Player;
import com.google.common.collect.Sets;

public class WalletManager {
    private final Logger log = LoggerFactory.getLogger(WalletManager.class);
	private BigDecimal balanceLimit;
	private Set<String> blacklist;
    private Map<String, Wallet> wallets = new ConcurrentHashMap<>();

    @Autowired
	private PlayerRepository repo;
	
	public void setBalanceLimit(BigDecimal balanceLimit) {
		this.balanceLimit = balanceLimit;
	}

	/**
	 * Blacklist is provided as players divided by commas: player1,player2,player3,etc...
	 * @param blacklist
	 */
	public void setBlacklist(String blacklist) {
		this.blacklist = Sets.newHashSet(Arrays.asList(blacklist.split(",")));
	}

    /**
     * Adds transaction to users' wallet
     *
     * @param request
     * @return
     */
    public WalletManagerServiceResponse addTransaction(TransactionRequest request){
        final WalletManagerServiceResponse response = new WalletManagerServiceResponse();
        if (blacklist.contains(request.getUsername())){
            response.setError(TransactionError.BLACKLISTED_USER);
        } else if (balanceLimit.compareTo(request.getChange().abs()) < 0){
            response.setError(TransactionError.BALANCE_LIMIT_BREACH);
        } else {
        	//Synchronization is needed here as wallet wallet can be updated concurrently, e.g. wallet balance may be not relevant to current transaction
        	synchronized (request.getUsername().intern()){
        		final Wallet wallet = getWallet(request.getUsername());
                final TransactionError error = wallet.addTransaction(request.getTransactionId(), request.getChange());
                if (error == null || error == TransactionError.TRANSACTION_DUPLICATE) {
                    response.setBalance(wallet.getAmount());
                    response.setVersion(wallet.getVersion());
                    response.setError(error);
                } else {
                    response.setError(error);
                }
            }
        }
        return response;
    }
    
    public Map<String, Wallet> getWallets(){
    	return wallets;
    }

    private Wallet getWallet(String username){
        if (!wallets.containsKey(username)){
        	Wallet wallet = getWallet();
            final Player player = repo.findByUsername(username);
            if (player != null){
            	wallet.setAmount(player.getBalance());
            	wallet.setVersion(player.getVersion());
                log.info("Wallet loaded for user {}, balance {} version {}", username, wallet.getAmount(), wallet.getVersion());
            } else {
                log.info("New wallet created for user {}", username);
            }
            wallets.put(username, wallet);
        }
        return wallets.get(username);
    }
    
    @Lookup
    public Wallet getWallet(){
    	throw new UnsupportedOperationException("This method must be replaced by Spring Proxy");
    }
	
}
