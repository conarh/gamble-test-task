package com.conarh.gamble.walletserver.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="player")
public class Player{
	
	@Id
	@Column(name="player_id", nullable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="username", nullable=false, unique=true)
	private String username;
	
	@Column(name="balance_version", nullable=false)
	private long version = 0;
	
	@Column(name="balance", columnDefinition="DOUBLE", precision=19, scale=2, nullable=false)
	private BigDecimal balance;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public long getVersion() {
		return version;
	}
	
	public void setVersion(long version) {
		this.version = version;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public long getId() {
		return id;
	} 

}
